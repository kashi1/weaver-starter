import * as React from 'react'
import {TopMenu, TopMenuItem} from '@simplus/siui'
import {NavLink} from 'react-router-dom'
export class NavBar extends React.Component {
	render(): JSX.Element {
		return<div><TopMenu style={{
					height: '70px'
					}}>
				<NavLink activeClassName='active' exact to='/'>
				<TopMenuItem className='menu-item'>Home</TopMenuItem>
				</NavLink>
				<NavLink activeClassName='active' to='/task-1'>
				<TopMenuItem className='menu-item'>Task 1</TopMenuItem>
				</NavLink>
				<NavLink activeClassName='active' to='/task-2'>
				<TopMenuItem className='menu-item'>Task 2</TopMenuItem>
				</NavLink>
			</TopMenu>
			</div>
	}
}