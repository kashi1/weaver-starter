import * as React from 'react'
import {NavBar} from './NavBar'
import {HashRouter as Router, Route, Switch} from 'react-router-dom';

export const NavBarApp = () => (
	<Router>
		<Switch>
			<Route path='/' component={NavBar} />
		</Switch>
	</Router>
)