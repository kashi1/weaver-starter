import * as React from 'react';
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import {GridLayoutPage} from './home';
import {Task1} from './task1';
import {Task2} from './task2';
/* tslint:disable */
const settings = require('../../settings/client.json')

export class App extends React.Component {
	componentDidMount(): void {
		document.title = settings.name;
	}
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		return <div className='app-container'>
			<Router>
				<Switch>
					<Route exact path='/' component={GridLayoutPage} />
					<Route exact path='/task-1' component={Task1} />
					<Route exact path='/task-2' component={Task2} />
				</Switch>
			</Router>
		</div>
	}
}
export default App;