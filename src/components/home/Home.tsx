import * as React from "react"
import { Select,DatePicker, MultiSelect, MultiSelectOption as MultiOption,Card,IconAndName,ProfilePicture,Switch,Slider,Input } from "@simplus/siui"
import {GridLayout} from "@simplus/macaw-business"
import { Icon } from "antd"
import * as moment from 'moment'
import * as _ from 'lodash'

const data = require('./taskdata.json')
const Option = Select.Option;
const Search = Input.SearchInput;
export class GridLayoutPage extends React.Component<any,any>{
	state = {
		items:data,
		borderless: false,
		showLabel: true,
		label:"My Company",
		showAllText: true,
		allSelectedText:"All Companies",
		matchWidth: true,
		disabled: false,
		tooltip: undefined,
		openUp: false,
		invertSelection: false,
		hideSelected:false
	};
	onChange(checked) {
		if(!checked){
			const result = data.filter(data => data.active );
			this.setState({items: result})
		}else{
			this.setState({items: data})
		}
	  }
	  onSearch(value) {
		if(value){
			// const result = data.filter(data => data.name.toLowerCase().includes(value.toLowerCase()));
			const result = data.filter(data => new RegExp(value, 'i').test(data.name));
			this.setState({items: result})
		}else{
			this.setState({items: data})
		}
	  }

	  sliderChange(value) {
		const result = data.filter(data =>{
			return (data.assets >= value[0]  && data.assets <= value[1])
	} );
		this.setState({items: result})
	  }
	  
	  dateChange(dates) {
		const min=dates.startDate.format('lll');
		const max=dates.endDate.format('lll');
		const result = data.filter(data =>{
			return (moment(data.date).format('lll') >= min  || moment(data.date).format('lll') <= max)
	} );
		this.setState({items: result})
	  }
	  options:any=[];
	
	  onChangeMulti = (selected) => {
		  let output = selected.value
		  if(this.state.invertSelection)
		  {
			  output = _.differenceWith(this.options, selected.value, _.isEqual);
		  }
		  console.log(output);
		  const result = data.filter(data =>  output.indexOf(data.name)>-1 );
		  this.setState({items: result})
		  if(!output.length){
			this.setState({items: data})
		  }
	  }
	  onMenuClosed = (selected) => {
		  let output = selected.value
		  if(this.state.invertSelection)
		  {
			  output = _.differenceWith(this.options, selected.value, _.isEqual);
		  }
		  console.log(output);
	  }
	  
    render(){
		for (let index = 0; index < data.length; index++) {
			this.options.push(data[index].name);
		}
			let placeholder = "Select your Company";
			if(this.state.invertSelection)
				placeholder = "Choose items to be excluded"
		const topMenu = (<div style={{display:"flex", alignItems:"center", padding:"1rem", marginBottom:10, height:50, background:"#DDD"}}>
                <MultiSelect
					width={250} 
					label={this.state.label}  
					// allSelectedText={this.state.showAllText?(items)=>this.state.allSelectedText+" ("+items+")":null} 
					customSelectedText="countries"
					borderless={this.state.borderless} 
					tooltip={this.state.tooltip}
					openUp={this.state.openUp}
					hideSelected={this.state.hideSelected}
					disabled={this.state.disabled}
					onChange={this.onChangeMulti}
					onMenuClosed={this.onMenuClosed}
					placeholder={placeholder}>
					{this.options.map( (values, index)=> <MultiOption key={index} value={values}>{values}</MultiOption>)}
				</MultiSelect>
			{/* </div> */}
				<DatePicker 
			label="DatePicker: "
			tooltip="Data available from Jan 20 - Jan 28" 
			customOption
			// onChange={dates=>alert("From: "+dates.startDate.format('lll')+" , To: "+dates.endDate.format('lll'))} 
			onChange={this.dateChange.bind(this)} 
			style={{minWidth: 100}} 
			placeholder="Select a date">
			<Option value = "past hour">Past Hour</Option>
			<Option value = "past day">Past Day</Option>
			<Option value = "past 7 days">Past Week</Option>
			<Option value = "past month">Past Month</Option>
		</DatePicker>
				<Switch tooltip="tooltip" label={(checked)=>"Value: "+checked} defaultChecked onChange={this.onChange.bind(this)} />
				<Slider label={(value)=>"Slider Values: "+value} tooltip="Uses callback function as label" onAfterChange={this.sliderChange.bind(this)} min={100} max={10000} range defaultValue={[20, 100]} />
				<Search label="Filter" placeholder="search by company name" onSearch={this.onSearch.bind(this)}/>
			</div>)
        return <GridLayout header={topMenu} spacebetween={10} maxColumns={3} columns={2} minColumns={1} style={{height:"30rem", margin:20, background:"#EEE"}}>
					{
          this.state.items.map(function(item) {
            return (
                <Card padding key={item.id} >
			<div style={{ display:"flex"}}>
			<div style={{ float:"left"}}>
			<ProfilePicture url={item.logo} margin size={100} rounded outstand/>
			</div>
			<div style={{ float: 'right'}}>
			<IconAndName icon={<Icon type="user"/>} name={item.name} />
			<IconAndName icon={<Icon type="profile"/>} name={item.assets} />
			<IconAndName icon={<Icon type="calendar"/>} name={item.date} />
			<IconAndName icon={<Icon type="tags-o"/>} name={item.category} />
			<IconAndName icon={<Icon type="check"/>} name={item.active? "true":"false"} />
			</div>
			</div>
			</Card>
            );
          })
        }
				</GridLayout>
    }
}

export default GridLayoutPage