import * as React from "react"
import {Table} from "antd"
import {Card, Button,Notification,Input  } from "@simplus/siui"
import {TableCardComboLayout} from "@simplus/macaw-business"
const data = require('./task1.json')

export class Task1 extends React.Component<any,any>{
	state={
		shownotif:false,
		items:data,
		cardData: []
	}
	Title = () => {
	return (
		<span>Sample Table Title</span>
	)
}
onSearch = (value) => {
	const result = data.filter(data => new RegExp(value, 'i').test(data.first_name));
			this.setState({items: result},() => {
				console.log(this.state.items, 'dealersOverallTotal1');
			  })
}

btnTapped(){
this.setState({shownotif:true});
setTimeout(() => { this.setState({shownotif:false}) }, 5000);
}
	render(){
		const sample_table = <Table
title={this.Title}
onRow={(record) => ({
	onClick: () => {
		const arr:any=[];
		arr.push(record.first_name);
		arr.push(record.last_name);
		arr.push(record.role);
		arr.push(record.email);
	this.setState({cardData: arr})
	},
  })}
	columns={[
		{
			dataIndex : "first_name",
			key : "first_name",
			title: (
                <div style={{ textAlign: 'center' }}>
                    <div>First Name</div>
                    <hr />
                    <Input placeholder='Search' onChange={this.onSearch} />
                </div>
            ),
			sorter: (a:any, b:any) => a.first_name.length - b.first_name.length,
			width: 100
		},
		{
			dataIndex : "last_name",
			key : "last_name",
			title: "Last Name",
			sorter: (a:any, b:any) => a.last_name.length - b.last_name.length,
			width: 100
		},
		{
			dataIndex : "email",
			key : "email",
			title : "Email",
			sorter: (a:any, b:any) => a.email.length - b.email.length,
			width: 300
		},
		{
			dataIndex : "role",
			key : "role",
			title : "role",
			sorter: (a:any, b:any) => a.role.length - b.role.length,
			width: 100
		}
	]}
	pagination={{total : this.state.items.length,pageSize : 10}}
	dataSource={this.state.items}>

</Table>
		return	<TableCardComboLayout
		padding
		table={sample_table}
		style={{display: 'flex'}}>
			<div className="col-8">
			<Button size="jumbo" style={{width:280, height:60}} onClick={this.btnTapped.bind(this)}>Be Lekker</Button>
			{ this.state.shownotif?<Notification visible> Standard Notification disappears after 3 second </Notification> : null }
			<Card
				rounded
				className="default">
				<div className="default-message">
				<ul>
				{this.state.cardData.map((number,index) =>  <li key={index}>{number}</li>)}
				</ul>
				</div>
			</Card>
			</div>
	</TableCardComboLayout>
	}
}
export default Task1
