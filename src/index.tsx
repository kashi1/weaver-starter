import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'style'
import 'antd/dist/antd.less';
import '@simplus/siui/style/index.less'

import {App} from './components';
import {NavBarApp} from './components/navigation';

ReactDOM.render(<NavBarApp/>, document.getElementById('nav'))
ReactDOM.render(<App/>, document.getElementById('my-awesome-app'))